tool
extends EditorPlugin


const TrackEditorGizmoPlugin = preload("res://addons/track_editor/track_editor_gizmo_plugin.gd")

var gizmo_plugin = TrackEditorGizmoPlugin.new()


func _enter_tree():
	add_spatial_gizmo_plugin(gizmo_plugin)


func _exit_tree():
	remove_spatial_gizmo_plugin(gizmo_plugin)
	

