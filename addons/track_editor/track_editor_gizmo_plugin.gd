tool
extends EditorSpatialGizmoPlugin

enum HandleType{ POSITION, IN, OUT, TILT }

export(float) var up_vector_size = 1 # meters



var _previous_handle_id:int = -1
var _previous_handle_distance:float = 0.0




func _init():
	create_material("path", Color(1,1,1))
	create_material("bezier_lines", Color(0.2,0.25,0.3))
	create_material("tilt_lines", Color(0.2,0.5,0.5))
	create_handle_material("handles")

	
func get_edit_point_index( curve,  index ):
	var n_points = curve.get_point_count()
	return index % n_points
	
	

func get_handle_value(gizmo, index):
	var path = gizmo.get_spatial_node() as Path
	var curve = path.curve
	var handle_type = get_handle_type(curve, index)
	var edit_point_index = get_edit_point_index(curve, index)
	match(handle_type):
		HandleType.POSITION:
			return curve.get_point_position(index)
		HandleType.TILT:
			return curve.get_point_tilt(edit_point_index)
		HandleType.IN:
			return curve.get_point_in(edit_point_index)
		HandleType.OUT:
			return curve.get_point_out(edit_point_index)

func get_handle_name(gizmo, index):
	var path:Path = gizmo.get_spatial_node() as Path
	var curve:Curve3D = path.curve as Curve3D
	var handle_type = get_handle_type(curve, index)
	var handle_name = "["+ str(index) +"]"
	match(handle_type):
		HandleType.POSITION:
			handle_name = "CurvePoint" + handle_name
		HandleType.IN:
			handle_name = "In" + handle_name
		HandleType.OUT:
			handle_name = "Out" + handle_name
		HandleType.TILT:
			handle_name = "Tilt" + handle_name
	return handle_name
	
func get_handle_type( curve, index):
	var n_points = curve.get_point_count()
	if index < n_points:
		return HandleType.POSITION
	elif index < n_points *2:
		return HandleType.TILT
	elif index < n_points *3:
		return HandleType.IN
	else:
		return HandleType.OUT
	
func get_name():
	return "TrackGizmo"

func has_gizmo(spatial):
	return spatial is Path


func redraw(gizmo):
	gizmo.clear()

	# Get common info
	var path:Path = gizmo.get_spatial_node() as Path
	var curve:Curve3D = path.curve as Curve3D
	var n_points:int = curve.get_point_count()
	var baked_points:PoolVector3Array = curve.get_baked_points()
	var baked_up_vectors:PoolVector3Array = curve.get_baked_up_vectors()
	
	# Main path
	var last_point = null
	var path_lines:PoolVector3Array = PoolVector3Array()
	for current_point in baked_points:
		if last_point != null:
			path_lines.push_back(last_point)
			path_lines.push_back(current_point)
		last_point = current_point
	gizmo.add_lines(path_lines, get_material("path"))
	
	
	var handles:PoolVector3Array = PoolVector3Array()
	var tilt_handles:PoolVector3Array = PoolVector3Array()
	var tilt_lines:PoolVector3Array = PoolVector3Array()
	var in_handles:PoolVector3Array = PoolVector3Array()
	var out_handles:PoolVector3Array = PoolVector3Array()
	var in_lines:PoolVector3Array = PoolVector3Array()
	var out_lines:PoolVector3Array = PoolVector3Array()
	
	
	# Edit points
	for i in range(0, n_points):
		var point:Vector3 = curve.get_point_position(i)
		var tilt:float = curve.get_point_tilt(i)
		var point_in:Vector3 = curve.get_point_in(i)
		var point_out:Vector3 = curve.get_point_out(i)
		var tangent:Vector3 = point_out - point
		var closest_offset = curve.get_closest_offset( point )
		var closest_up_vector = curve.interpolate_baked_up_vector(closest_offset, true)
		
		# Edit point
		handles.push_back(point)
		
		# Tilt handle
		var tilt_handle_position:Vector3 = point + closest_up_vector * up_vector_size
		
		
		tilt_handles.push_back(tilt_handle_position)
		tilt_lines.push_back( point )
		tilt_lines.push_back( tilt_handle_position )
		
		# Bezier handles
		in_handles.push_back(point_in)
		out_handles.push_back(point_out)
		in_lines.push_back(point)
		in_lines.push_back(point_in)
		out_lines.push_back(point)
		out_lines.push_back(point_out)
		
		
			
	gizmo.add_handles(handles, get_material("handles") )
	gizmo.add_handles(tilt_handles, get_material("handles") )
	gizmo.add_lines( tilt_lines, get_material("tilt_lines") )
#	gizmo.add_handles(in_handles, get_material("handles") )
#	gizmo.add_handles(out_handles, get_material("handles") )
#	gizmo.add_lines(in_lines, get_material("bezier_lines") )
#	gizmo.add_lines(out_lines, get_material("bezier_lines") )
	
	
	
			
	
func set_handle(gizmo, index, camera, point):
	var path = gizmo.get_spatial_node() as Path
	var curve = path.curve
	var handle_type = get_handle_type(curve, index)
	var edit_point_index = get_edit_point_index(curve, index)
	
	match(handle_type):
		HandleType.POSITION, HandleType.IN, HandleType.OUT:
			var handle_position = get_handle_value(gizmo, index)
			var handle_screen_position = camera.unproject_position(handle_position)
			if index != _previous_handle_id:
				_previous_handle_id = index
				_previous_handle_distance  = camera.global_transform.origin.distance_to(handle_position)
			
			var new_position = camera.project_position( point, _previous_handle_distance )
			match(handle_type):
				HandleType.POSITION:
					curve.set_point_position( edit_point_index, new_position)
				HandleType.IN:
					curve.set_point_in(edit_point_index, new_position)
				HandleType.OUT:
					curve.set_point_out(edit_point_index, new_position)
		HandleType.TILT:
			var screen_center:Vector2 = camera.get_viewport().get_visible_rect().size / 2
			var angle = screen_center.angle_to_point(point)
			curve.set_point_tilt(edit_point_index, angle)
			
	#print("Set value:" +get_handle_name(gizmo, index) )
	path.emit_signal("curve_changed")
	
	# Update path follow children
	for c in path.get_children():
		if c is PathFollow:
			c.offset = c.offset
	
	
	
