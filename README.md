# Track Editor Plugin for Godot

Tools to create a track from a 3D curve. Mostly useful for racing games.

First created for the [Paleolympics prototype](https://youtube.com/playlist?list=PLXakhlcNXuRx2KFIaz98EQ8sU_ZhzS7Zd)


## Roadmap
First version is a workaround and it won't be developed further until december 2021.
* Add tilt indicators placed at baked points
* Add in and out handles
* Add lenght display
